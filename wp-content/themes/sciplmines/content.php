
<div class="page-title">
        <div class="container">
          <div class="row">
            <h1><?php the_title(); ?></h1>
          </div>
        </div>
    </div>
    <div class="main">
    	<?php the_content(); ?>
	</div>