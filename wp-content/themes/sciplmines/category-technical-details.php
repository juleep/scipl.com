
<?php
   /**
   * A Simple Category Template
   */
    if (is_category()) {
        $this_category = get_category($cat);
           // print_r($this_category);die;
    }
    $sub_cat = $sub_category = get_categories( array( 'orderby' => 'name', 'child_of' => $cat, 'hide_empty' => 0, 'parent' => $cat ) );
    $parent_cat_id = get_cat_ID( 'services' );
      
    $main_category = get_categories( array( 'orderby' => 'name', 'exclude' => $cat, 'hide_empty' => 0, 'parent' =>$parent_cat_id ) );
    $cat_data_1 = get_option('category_'.$this_category->cat_ID);
    // print_r($cat_data_1);die;
    $other_sub_cat = get_categories( array( 'orderby' => 'name', 'exclude' => $this_category->cat_ID, 'hide_empty' => 0, 'parent' =>$this_category->parent ) );
    get_header();?>

    <div class="page-title">
        <div class="container">
            <div class="row">
                <h1><?php echo $this_category->name;?></h1>
                <p><?php echo $cat_data_1['sub_title_description'];?></p>
            </div>
        </div>
    </div>
	<div class="main">
        <section class="all-section">
            <div class="container">
          	    <?php if (empty($sub_category)) { 
	               $sub_category = $other_sub_cat;
	               }
	            ?> 
                <div class="row">
          	        <?php 
                    foreach ($sub_category as $subcat) {
                        $cat_data = get_option('category_'.$subcat->cat_ID);
                        // print_r($cat_data['sub_title']);die;
                    ?>
                    <div class="col-lg-4 col-sm-4">
                        <div class="technical-box">
                        <?php
                            if (function_exists('get_wp_term_image'))
                            {
                                $meta_image = get_wp_term_image($subcat->term_id); 
                            }
                        ?>
                            <img src="<?php echo $meta_image; ?>" alt="01">
                            <div class="left-border-box">
                                <p><?php echo $subcat->cat_name; ?></p>
                                <h4><?php echo $cat_data['sub_title'];?> <span><?php echo $cat_data['sub_title_description'];?></span></h4>
                            </div>  
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </section>
        <section class="all-section grey-bg">
            <div class="container">
                <div class="row">
                    
                    <div class="col-lg-4 col-sm-4">
                        <div class="custom-tabel">
                            <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                      <th scope="col">Granite Block-3</th>
                                      <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                      <td>Region</td>
                                      <td><strong>Rangpur</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Abrasion Value</td>
                                      <td><strong>17.02</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Bulk density</td>
                                      <td><strong>2.71</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Compressive Strength</td>
                                      <td><strong>1185.07 kg/cm2</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Flexural Strength</td>
                                      <td><strong>147.31 kg/cm2</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Polishing Index</td>
                                      <td><strong>(-0.012, +0.001)</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="custom-tabel">
                            <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                      <th scope="col">Granite Block-2</th>
                                      <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                      <td>Region</td>
                                      <td><strong>Rangpur</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Abrasion Value</td>
                                      <td><strong>16.62</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Bulk density</td>
                                      <td><strong>2.73</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Compressive Strength</td>
                                      <td><strong>1357.81 kg/cm2</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Flexural Strength</td>
                                      <td><strong>83.47 kg/cm2</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Polishing Index</td>
                                      <td><strong>(-0.010, +0.003)</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="custom-tabel">
                            <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                      <th scope="col">Granite Block-3</th>
                                      <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                      <td>Region</td>
                                      <td><strong>Rangpur</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Abrasion Value</td>
                                      <td><strong>16.79</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Bulk density</td>
                                      <td><strong>2.73</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Compressive Strength</td>
                                      <td><strong>235.68 kg/cm2</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Flexural Strength</td>
                                      <td><strong>83.45 kg/cm2</strong></td>
                                    </tr>
                                    <tr>
                                      <td>Polishing Index</td>
                                      <td><strong>(-0.004, +0.006)</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12">
                        <ol>
                          <li>* All images are indicative.</li>
                          <li>* This information is based on test results
                          from government and independent test labs.
                          However, this shall not constitute a guarantee for
                          a specific batch of stone, and shall not establish a
                          legally valid contractual relationship.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>
