<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/assets/images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo get_template_directory_uri();?>/assets/images/favicon.ico" type="image/x-icon">
	<!-- <link href="<?php echo get_bloginfo( 'template_directory' );?>/assets/css/blog.css" rel="stylesheet"> -->
	<link href="<?php echo get_template_directory_uri();?>/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri();?>/assets/css/main.css" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<script src="<?php echo get_bloginfo( 'template_directory' );?>/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

	<?php wp_head();?>
</head>

<body <?php if(!is_front_page()){body_class(array('inner-page',get_post_field( 'post_name', get_post() )));}else{body_class();} ?>>
 <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header>
		     <div class="container">
			    <nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="/about-us/"><img src="<?php echo get_bloginfo( 'template_directory' );?>/assets/images/logo.svg"></a>
					 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
					    	<span class="navbar-toggler-icon"></span>
					</button>
				  	<div class="collapse navbar-collapse" id="navbarText">
					    <?php wp_nav_menu( array( 'theme_location' => 'new-menu' ) ); ?>
				  	</div>
				</nav>
			</div>
		</header>
	<!-- <div class="blog-masthead">
		<div class="container">
			<nav class="blog-nav">
				<?php wp_nav_menu( array( 'theme_location' => 'new-menu' ) ); ?>
			</nav>
		</div>
	</div> -->
	
	

		