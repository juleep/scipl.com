   
		<footer>
          <div class="footer-top">
            <div class="container">
              <div class="row">
                <div class="cpl-lg-6 col-sm-6">
                  <p>bSafal house B/h.Mirch Masala Restaurant, Off S. G. Highway,<span>
                    Ahmedabad -380059, Gujarat (INDIA)</span></p>
                </div>
                <div class="cpl-lg-6 col-sm-6">
                  <ul class="contact-details">
                    <li><a href="mailto:sales.mines@bsafal.com">sales.mines@bsafal.com</a></li>
                    <li><a href="tel:+91 79 6190 0900">+91 79 6190 0900</a></li>
                  </ul>
                </div>
              </div>
            </div>
                </div>
            <div class="clear"></div>
            <div class="container">
              <div class="row">
                <h6>© SAFAL CONSTRUCTIONS INDIA PVT. LTD. 2019 ALL RIGHTSRESERVED</h6>
              </div>
            </div>
      
        </footer>

    <script src="<?php echo get_template_directory_uri();?>/assets/js/vendor/jquery-1.11.2.min.js"></script>
     <script src="<?php echo get_template_directory_uri();?>/assets/js/vendor/bootstrap.min.js"></script>
     <script src="<?php echo get_template_directory_uri();?>/assets/js/main.js"></script>
    <?php wp_footer(); ?> 
    </body>
</html>