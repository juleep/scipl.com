<?php
function register_my_menus() {
  register_nav_menus(
    array(
      'new-menu' => __( 'New Menu' ),
      'another-menu' => __( 'Another Menu' ),
      'an-extra-menu' => __( 'An Extra Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );


// widget in wordpress

if (function_exists('register_sidebar')) {
register_sidebar(array(
'name' => 'HomeLeft Widgets',
'id'   => 'homeleft-widgets',
'description'   => 'Widget Area',
'before_widget' => '<div id="one" class="two">',
'after_widget' => '</div>',
'before_title' => '<h2>',
'after_title'   => '</h2>'
));
}


/*-- custom menu class --*/
function add_classes_on_li($classes, $item, $args) {
  $classes[] = 'nav-item';
  return $classes;
}
add_filter('nav_menu_css_class','add_classes_on_li',1,3);

add_filter( 'nav_menu_link_attributes', 'wpse156165_menu_add_class', 10, 3 );

function wpse156165_menu_add_class( $atts, $item, $args ) {
    $class = 'nav-link'; // or something based on $item
    $atts['class'] = $class;
    return $atts;
}


/* add extra fields to category edit form hook */
add_action ( 'edit_category_form_fields', 'extra_category_fields');

//add extra fields to category edit form callback function
function extra_category_fields( $tag ) {    //check for existing featured ID
    $t_id = $tag->term_id;
    $cat_meta = get_option( "category_$t_id");
?>

<tr class="form-field">
<th scope="row" valign="top"><label for="sub_title"><?php _e('Sub Title'); ?></label></th>
<td>
<input type="text" name="Cat_meta[sub_title]" id="Cat_meta[sub_title]" size="25" style="width:95%;" value="<?php echo $cat_meta['sub_title'] ? $cat_meta['sub_title'] : ''; ?>"><br />
        <span class="description"><?php _e('Sub Title'); ?></span>
    </td>
</tr>

<tr class="form-field">
<th scope="row" valign="top"><label for="sub_title_description"><?php _e('Sub Title Description'); ?></label></th>
<td>
<input type="text" name="Cat_meta[sub_title_description]" id="Cat_meta[sub_title_description]" size="25" style="width:95%;" value="<?php echo $cat_meta['sub_title_description'] ? $cat_meta['sub_title_description'] : ''; ?>"><br />
        <span class="description"><?php _e('Sub Title For Description'); ?></span>
    </td>
</tr>

<?php
}

// save extra category extra fields hook
add_action ( 'edited_category', 'save_extra_category_fileds');

// save extra category extra fields callback function
function save_extra_category_fileds( $term_id ) {
    if ( isset( $_POST['Cat_meta'] ) ) {
        $t_id = $term_id;
        $cat_meta = get_option( "category_$t_id");
        $cat_keys = array_keys($_POST['Cat_meta']);
            foreach ($cat_keys as $key){
            if (isset($_POST['Cat_meta'][$key])){
                $cat_meta[$key] = $_POST['Cat_meta'][$key];
            }
        }
        //save the option array
        update_option( "category_$t_id", $cat_meta );
    }
}


add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}