
<?php
   /**
   * A Simple Category Template
   */
    if (is_category()) {
        $this_category = get_category($cat);
           // print_r($this_category);die;
    }
    $sub_cat = $sub_category = get_categories( array( 'orderby' => 'name', 'child_of' => $cat, 'hide_empty' => 0, 'parent' => $cat ) );
    $parent_cat_id = get_cat_ID( 'services' );
      
    $main_category = get_categories( array( 'orderby' => 'name', 'exclude' => $cat, 'hide_empty' => 0, 'parent' =>$parent_cat_id ) );
    $cat_data = get_option('category_'.$this_category->cat_ID);
    $other_sub_cat = get_categories( array( 'orderby' => 'name', 'exclude' => $this_category->cat_ID, 'hide_empty' => 0, 'parent' =>$this_category->parent ) );
    // print_r($sub_category);die;
    get_header();?>

    <div class="page-title">
        <div class="container">
            <div class="row">
                <h1><?php echo $this_category->name;?></h1>
                <p><?php echo $cat_data['sub_title_description'];?></p>
            </div>
        </div>
    </div>
  <div class="main">
        <section class="all-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-sm-7">
                        <div class="content">
                            <h2>The iconic finish</h2>
                            <p>
                                Dealing with market demands SCIPL caters to different requirements with its 6 iconic finishes which
comprise of Chico Brown, Lappato Finish, River Finish, Short Blast with Brush Epoxy, Plain Epoxy and
Colour Epoxy (Brown).
                            </p>
                            <p>
                                These flawless finishes are not just cosmetic, but will also change the surface tendency in regards with
stain and how often polishing would be required.
                            </p>
                            <p>
                               The finishes come in different formats where they can be polished, brushed, flamed or honed and each
method gives a unique texture and feel to the stone. From an elegant and classy look these will also be
easy to maintain.
                            </p>
                            <p>
                               These finishes are visually appealing and will highlight all the different hues and shades of the stone.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-5 col-sm-5">
                        <div class="content-img">
                            <img src="<?php echo get_bloginfo( 'template_directory' );?>/assets/images/iconic-finish.jpg;?>" alt="about us">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php 
        // print_r(count($sub_category));die;
          $counter = 1;
          for ($i = 0 ; $i < count($sub_category) ; $i++) {
// echo "string";
            $sub_subcategory_1 = get_categories( array( 'orderby' => 'term_order', 'child_of' => $sub_category[$i]->term_id, 'hide_empty' => 0, 'parent' => $sub_category[$i]->term_id ) );

            
            $cat_data_sub = get_option('category_'.$sub_category[$i]->cat_ID);
            if(($counter % 2) == 1) {
              $section_class = "grey-bg";
              $section_side = "left-content";
            }
            else {
              $section_class = "";
              $section_side = "right-content";
            }
            // print_r($cat_data_sub);die;
        ?>
        <section class="all-section <?php echo $section_class; ?>">
            <div class="container">
                <div class="row">
                    <?php 
                        if(($counter % 2) == 1) {
                          ?>
                            <div class="col-lg-4 col-sm-6">
                                <div class="content-img">
                                    <?php
                                        if (function_exists('get_wp_term_image'))
                                        {
                                            $meta_image = get_wp_term_image($sub_category[$i]->term_id); 
                                        }
                                    ?>
                                    <img src="<?php echo $meta_image;?>">
                                </div>
                            </div>
                            <div class="col-lg-8 col-sm-6">
                                <div class="iconic-product-dec left-content">
                                    <p><strong><?php echo $sub_category[$i]->name; ?></strong></p>
                                        <h2><?php echo $cat_data_sub['sub_title']; ?> <span><?php echo $cat_data_sub['sub_title_description']; ?></span></h2>
                                    <p><?php echo $sub_category[$i]->description; ?></p>
                                </div>
                            </div>
                          <?php 
                        }
                        else {
                          ?>
                            <div class="col-lg-8 col-sm-6">
                                <div class="iconic-product-dec <?php echo $section_side; ?>">
                                    <p><strong><?php echo $sub_category[$i]->name; ?></strong></p>
                                        <h2><?php echo $cat_data_sub['sub_title']; ?> <span><?php echo $cat_data_sub['sub_title_description']; ?></span></h2>
                                    <p><?php echo $sub_category[$i]->description; ?></p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="content-img">
                                    <?php
                                        if (function_exists('get_wp_term_image'))
                                        {
                                            $meta_image = get_wp_term_image($sub_category[$i]->term_id); 
                                        }
                                    ?>
                                    <img src="<?php echo $meta_image;?>">
                                </div>
                            </div>
                          <?php
                        }
                    ?>

                    
                </div>
            </div>
        </section>
        <?php
          $counter = $counter + 1;
        }
      ?>
    </div>
<?php get_footer(); ?>
