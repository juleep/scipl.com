<?php
   /**
   * A Simple Category Template
   */
   $this_category == '';
   if (is_category()) {
           $this_category = get_category($cat);
           // print_r($this_category);die;
       }
   
    $sub_cat = $sub_category = get_categories( array( 'orderby' => 'name', 'child_of' => $cat, 'hide_empty' => 0, 'parent' => $cat ) );
           $parent_cat_id = get_cat_ID( 'services' );
          
          $main_category = get_categories( array( 'orderby' => 'name', 'exclude' => $cat, 'hide_empty' => 0, 'parent' =>$parent_cat_id ) );
          $other_sub_cat = get_categories( array( 'orderby' => 'name', 'exclude' => $this_category->cat_ID, 'hide_empty' => 0, 'parent' =>$this_category->parent ) );
       get_header();?>
    <div class="main">
       <div class="subheader" style="background-image: url('<?php echo $cat_data['banner_image_url']; ?>');">
          <div class="container">
             <div class="row">
                <div class="col-lg-12">
                   <h1><?php echo $this_category->cat_name; ?></h1>
                   <p><?php echo $cat_data['sub_title']; ?></p>
                </div>
             </div>
          </div>
       </div>
       <!-- white part start here -->
       <section class="service diamond-shape">
          <!-- left image part start here -->
          <div class="container">
             <div class="row">
                <div class="col-lg-12">
                   <div class="space"></div>
                   <h3><?php echo $cat_data['sub_title_description']; ?></h3>
                   <?php echo $cat_data['long_description']; ?>
                </div>
             </div>
          </div>
          <div class="space"></div>
       </section>
       <!-- purple part start here -->
       <?php if (empty($sub_category)) { 
          $sub_category = $other_sub_cat;
          }
          ?> 
       <section class="service purple-bg diamond-shape">
          <!-- heading start here -->
          <div class="heading">
             <h2>Our Expertise</h2>
          </div>
          <div class="container">
             <div class="row justify-content-md-center">
                <?php 
                   foreach ($sub_category as $subcat) {
                   ?>
                <div class="col-lg-4 col-md-4 col-sm-4 text-center">
                   <div class="tag-box">
                      <a href="<?php echo get_category_link($subcat->term_id); ?>">
                         <span class="tag-icon-box"><img src="<?php
                            echo do_shortcode(sprintf('[wp_custom_image_category onlysrc="true" term_id="%s"]',$subcat->term_id));
                            ?>" alt="<?php echo $subcat->cat_name; ?>"></span>
                         <h4><?php echo $subcat->cat_name; ?></h4>
                      </a>
                   </div>
                </div>
                <?php
                   }
                   ?>
             </div>
          </div>
       </section>
       
       
    </div>
    <?php get_footer(); ?>
